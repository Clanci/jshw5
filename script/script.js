function createNewUser() {
    const newUser = {
        firstName: null,
        secondName: null,

        get firstName(){
            return this._firstName;
        },
        get secondName(){
            return this._secondName;
        },

        addingFirstName: function () {
            while (true) {
                const newFirstName = prompt("Введіть своє ім'я");
                if (newFirstName !== "" && !/\d/.test(newFirstName) && newFirstName[0] === newFirstName[0].toUpperCase()) {
                    this._firstName = newFirstName;
                    break;
                } else if (newFirstName === "") {
                    alert("Ім'я не має бути порожнім");
                } else if (newFirstName[0] !== newFirstName[0].toUpperCase()) {
                    alert("Ім'я має починатись з великої літери");
                } else if (/\d/.test(newFirstName)) {
                    alert("Ім'я не має містити цифри");
                }
            }
        },

        addingSecondName: function(){
            while (true) {
                const newSecondName = prompt("Введіть своє прізвище");
                if (newSecondName !== "" && !/\d/.test(newSecondName) && newSecondName[0] === newSecondName[0].toUpperCase()) {
                    this._secondName = newSecondName;
                    break;
                } else if (newSecondName === ""){
                    alert("Прізвище не має бути порожнім");
                } else if (/\d/.test(newSecondName)){
                    alert("Прізвище не має містити цифри");
                } else if (newSecondName[0] !== newSecondName[0].toUpperCase()){
                    alert("Прізвище має починатись з великої літери");
                }
            }
        },

        getLogin: function() {
            console.log(`Ваш логін: ${this._firstName[0].toLowerCase()}${this._secondName.toLowerCase()}`);
        }
    }

    newUser.addingFirstName();
    newUser.addingSecondName();
    newUser.getLogin();

    for (let key in newUser) {
        if (key === 'firstName' || key === 'secondName'){
            document.write(`<br><strong>${key}:</strong> ${newUser[key]}<br>`);
        }
    }
}

createNewUser();

